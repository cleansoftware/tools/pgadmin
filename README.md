# Pgadmin4



| Variable                 | Valor por defecto           | Links                                                |
|:-------------------------|-----------------------------|:-----------------------------------------------------|
| TRAEFIK_TAG              | v2.6                        | [Tags](https://hub.docker.com/_/traefik?tab=tags)    |
| TRAEFIK_URL              | traefik-local.midominio.com |                                                      |
| POSTGRES_TAG             | 13                          | [Tags](https://hub.docker.com/_/postgres?tab=tags)   |
| POSTGRES_USER            | postgres                    |                                                      |
| POSTGRES_PASSWORD        | mysecretpassword            |                                                      |
| PGADMIN_TAG              | 6.9                         | [Tags](https://hub.docker.com/r/dpage/pgadmin4/tags) |
| PGADMIN_DEFAULT_EMAIL    | myemail@mail.com            |                                                      |
| PGADMIN_DEFAULT_PASSWORD | mysecretpassword            |                                                      |
| PGADMIN_URL              | pgadmin-local.midominio.com |                                                      |
| PGADMIN_NAME_SERVICE     | PGADMIN_NAME_SERVICE        |                                                      |


## TRAEFIK_TAG

Version del traefik, en el momento de esta documentación se probó con exito la version v2.6 por lo que no se 
garantiza que funcione con diferente version.  

- [versiones](https://hub.docker.com/_/traefik?tab=tags)
- [documentacion](https://doc.traefik.io/traefik/)


## TRAEFIK_URL

Url donde se va a exponer la pagina web del dashboard de traefik

## POSTGRES_TAG

Version del postgres, en el momento de esta documentación se probó con exito la version 13 a diferencia que el traefik
por su naturaleza las versiones de postgres suelen ser mas estables y no suelen tener tantos problemas al subir versiones
no obstante no hay garantias

- [versiones](https://hub.docker.com/_/traefik?tab=tags)
- [documentacion](https://doc.traefik.io/traefik/)


## POSTGRES_USER
Usuario por defecto que se usara en el postgres para hacer login


## POSTGRES_PASSWORD

Password por defecto que se usara en el postgres para hacer login


## PGADMIN_TAG
Version del pgadmin, en el momento de esta documentación se probó con exito la version 6.9. En este caso al tratarse de una
interfaz grafica suele ser bastante estable a la hora de subir versiones, pero como siempre no hay garantias.

- [versiones](https://hub.docker.com/r/dpage/pgadmin4/tags)
- [documentacion](https://www.postgresql.org/docs/13/index.html)

## PGADMIN_DEFAULT_EMAIL
Email que se usa para hacer login en la interfaz del pgadmin

## PGADMIN_DEFAULT_PASSWORD
Password que se usa para hacer login en la interfaz del pgadmin

## PGADMIN_URL

Url donde se va a exponer la interfaz de pgadmin

## PGADMIN_NAME_SERVICE
Nombre que va a tomar el servicio en la interfaz del traefik para indetificarlo

#### Nota
Recuerda que todas las variables que hace referencia a XXX_URL es necesario indicarlos en el archivo /etc/hosts para que funcione
ya que de lo contrario no se aplicara la regla que activa a el traefik para redirigir correctamente al servicio correspondiente